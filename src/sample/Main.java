package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
    Stage window;

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        window = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/sample.fxml"));


        Image ikonka = new Image("icons/programIcon.png");
        window.getIcons().add(ikonka);
        window.setTitle("ORSR-Extractor");
        window.setScene(new Scene(root, 600, 400));
        window.show();

    }



}
