package sample;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;


public class AlertBox {
    public static void display(String message){
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);

        VBox layout = new VBox();
        layout.getChildren().add(new Label(message));
        layout.setAlignment(Pos.CENTER);


        Scene scene = new Scene(layout,300,300);




        window.setScene(scene);
        window.setTitle("About");
        window.showAndWait();
    }


}
