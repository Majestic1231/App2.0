package sample;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {

    public MenuItem zatvor, about, obchMeno, priezvisko, sidlo, spisZnacka, identCislo;
    public MenuButton searchingMenu, pravnaForma, sud;
    public MenuItem spat;
    public BorderPane content, podlaObchMena;
    public Button btn1, obch2;
    public TextField obchMenoText;
    public CheckBox check1, check2;
    static ObservableList<Firma>  zoznam = FXCollections.observableArrayList();


    public void closeProgram() {
        Platform.exit();
    }

    public static ObservableList<Firma> getZoznam(){
        return zoznam;
    }

    public void showAbout() {
        AlertBox.display("Created by Andrej Ščasný");
    }

    public void resetForm() {
        searchingMenu.setText("Typ vyhľadávania");
    }

    public void changeSearchingMenuText(ActionEvent event) {
        MenuItem source = (MenuItem) event.getSource();
        ContextMenu moj = source.getParentPopup();
        MenuButton menu = (MenuButton) moj.getOwnerNode();
        menu.setText(source.getText());

    }

    public void resetObchMeno() {
        pravnaForma.setText("");
        sud.setText("");
        obchMenoText.clear();
        check1.setSelected(false);
        check2.setSelected(true);
    }

    public void vyhladajObchMeno() {
        if (obchMenoText.getText().length() > 0) {
            zoznam = Extract.startExtract(new URL(obchMenoText.getText(), pravnaForma.getText(), sud.getText(), check1.isSelected(), check2.isSelected(), 1));
            if(zoznam != null)
            try {
                displayTable();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
            AlertBox.display("Málo znakov v položke Obchodné meno");
    }

    public void displayTable() throws IOException {
        Stage stage = new Stage();
        Parent root;

        //load up OTHER FXML document
        root = FXMLLoader.load(getClass().getResource("../fxml/table.fxml"));

        //create a new scene with root and set the stage
        Scene scene = new Scene(root,600,600);
        stage.setTitle("Zobrazenie výsledkov");
        stage.getIcons().add(new Image("icons/programIcon.png"));
        stage.setScene(scene);
        stage.show();


    }

    public void handleEnter(KeyEvent e) {
        if ((e.getCode().equals(KeyCode.ENTER))) {
            BorderPane layout = (BorderPane) e.getSource();
            if (layout == podlaObchMena) {
                vyhladajObchMeno();
            } else if (layout == content) {
                System.out.println("Switchujem content");
            }
        }
    }


    // Switching scenes(FXML)
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent root = null;
        if (event.getSource() == btn1 && (searchingMenu.getText() == obchMeno.getText())) {
            //get reference to the button's stage
            stage = (Stage) btn1.getScene().getWindow();
            //load up OTHER FXML document
            root = FXMLLoader.load(getClass().getResource("../fxml/obchodneMeno.fxml"));


            //create a new scene with root and set the stage
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
        if (event.getSource() == btn1 && (searchingMenu.getText() == identCislo.getText())) {
            //get reference to the button's stage
            stage = (Stage) btn1.getScene().getWindow();
            //load up OTHER FXML document
            root = FXMLLoader.load(getClass().getResource("../fxml/identifikacneCislo.fxml"));

            //create a new scene with root and set the stage
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
        if (event.getSource() == spat) {
            stage = (Stage) spat.getParentPopup().getOwnerNode().getScene().getWindow();
            root = FXMLLoader.load(getClass().getResource("../fxml/sample.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }

    }


}
