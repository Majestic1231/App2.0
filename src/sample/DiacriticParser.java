package sample;
import java.text.Normalizer;
import java.util.regex.Pattern;

public class DiacriticParser {
    public static String get(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        String retazec = pattern.matcher(nfdNormalizedString).replaceAll("");
        retazec = retazec.replaceAll(" ","+");
        retazec = retazec.replaceAll("[^a-zA-Z]", "");

        return retazec;
    }
}
