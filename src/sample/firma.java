package sample;


import javafx.beans.property.SimpleStringProperty;

public class Firma {
    private final SimpleStringProperty poradie;
    private final SimpleStringProperty name;
    private final SimpleStringProperty aktualny;
    private final SimpleStringProperty uplny;

    public Firma(String poradie, String name, String aktualny, String uplny) {
        this.poradie = new SimpleStringProperty(poradie);
        this.name = new SimpleStringProperty(name);
        this.aktualny = new SimpleStringProperty(aktualny);
        this.uplny = new SimpleStringProperty(uplny);
    }

    public String getPoradie() {
        return poradie.get();
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty poradieProperty() {
        return poradie;
    }

    public void setPoradie(String poradie) {
        this.poradie.set(poradie);
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public SimpleStringProperty aktualnyProperty() {
        return aktualny;
    }

    public void setAktualny(String aktualny) {
        this.aktualny.set(aktualny);
    }

    public SimpleStringProperty uplnyProperty() {
        return uplny;
    }

    public void setUplny(String uplny) {
        this.uplny.set(uplny);
    }

    public String getAktualny() {
        return aktualny.get();
    }


    public String getUplny() {
        return uplny.get();
    }

}
