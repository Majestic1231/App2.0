package sample;

public class URL {
    private String obchodneMeno;
    private String pravnaForma;
    private String sud;
    private boolean sposob;
    private boolean rozsah;
    private String adresa;
    private int volba;


    public URL(String obchodneMeno, String pravnaForma, String sud, boolean sposob, boolean rozsah, int volba) {
        this.obchodneMeno = obchodneMeno;
        this.pravnaForma = pravnaForma;
        this.sud = sud;
        this.sposob = sposob;
        this.rozsah = rozsah;
        this.volba = volba;
    }

    public int getVolba() {
        return volba;
    }

    public void setVolba(int volba) {
        this.volba = volba;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getPravnaForma() {
        return pravnaForma;
    }

    public void setPravnaForma(String pravnaForma) {
        this.pravnaForma = pravnaForma;
    }

    public String getSud() {
        return sud;
    }

    public void setSud(String sud) {
        this.sud = sud;
    }

    public boolean isSposob() {
        return sposob;
    }

    public void setSposob(boolean sposob) {
        this.sposob = sposob;
    }

    public boolean isRozsah() {
        return rozsah;
    }

    public void setRozsah(boolean rozsah) {
        this.rozsah = rozsah;
    }

    public String getObchodneMeno() {

        return obchodneMeno;
    }

    public void setObchodneMeno(String obchodneMeno) {
        this.obchodneMeno = obchodneMeno;
    }


}
