package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.ArrayList;
import java.io.IOException;


public abstract class Extract {
    public static ObservableList<Firma> startExtract(URL e) {
        switch (e.getVolba()) {
            case 1:
                return hladaj_subjekt(e);

            case 2:
                hladaj_ico(e);
                break;
            case 3:
                hladaj_sidlo(e);
                break;
            case 4:
                hladaj_spiszn(e);
                break;
            case 5:
                hladaj_osoba(e);
                break;
            default:
                break;
        }

        return null;
    }

    public static ObservableList<Firma> hladaj_subjekt(URL e) {
        e.setAdresa("http://orsr.sk/hladaj_subjekt.asp?OBMENO=&PF=0&SID=0&S=on&R=on");
        e.setObchodneMeno(DiacriticParser.get(e.getObchodneMeno()));
        e.setAdresa(e.getAdresa().replaceAll("OBMENO=", "OBMENO=" + e.getObchodneMeno()));
        if (e.getPravnaForma() != null) {
            e.setPravnaForma(getID_pravnaForma(e.getPravnaForma()));
            e.setAdresa(e.getAdresa().replaceAll("PF=0", "PF=" + e.getPravnaForma()));
        }
        if (e.getSud() != null) {
            e.setSud(getID_sud(e.getSud()));
            e.setAdresa(e.getAdresa().replaceAll("SID=0", "SID=" + e.getSud()));
        }
        if (!e.isSposob()) {
            e.setAdresa(e.getAdresa().replaceAll("&S=on", ""));
        }
        if (!e.isRozsah()) {
            e.setAdresa(e.getAdresa().replaceAll("&R=on", ""));
        }
        //Connectujem
       return ConnectParse(e.getAdresa());
    }

    public static void hladaj_ico(URL e) {
        e.setAdresa("http://orsr.sk/hladaj_ico.asp?ICO=&SID=0");
    }

    public static void hladaj_sidlo(URL e) {
        e.setAdresa("http://orsr.sk/hladaj_sidlo.asp?ULICA=&CISLO=&OBEC=&SID=0&R=on");
    }

    public static void hladaj_spiszn(URL e) {
        e.setAdresa("http://orsr.sk/hladaj_spiszn.asp?ODD=0&VLOZKA=&SID=0");
    }

    public static void hladaj_osoba(URL e) {
        e.setAdresa("http://orsr.sk/hladaj_osoba.asp?PR=&MENO=&SID=0&T=f0&R=on");
    }

    public static String getID_pravnaForma(String retazec) {
        switch (retazec) {
            case "štátny podnik":
                return String.valueOf(1);
            case "akciová spoločnosť":
                return String.valueOf(2);
            case "európska spoločnosť":
                return String.valueOf(17);
            case "spoločnosť s ručením obmedzeným":
                return String.valueOf(3);
            case "verejná obchodná spoločnosť":
                return String.valueOf(4);
            case "komanditná spoločnosť":
                return String.valueOf(5);
            case "samostatne podnikajúca fyz. osoba":
                return String.valueOf(6);
            case "družstvo":
                return String.valueOf(7);
            case "európske družstvo":
                return String.valueOf(19);
            case "európske zoskupenie hosp. záujmov":
                return String.valueOf(16);
            case "organizačná zložka podniku":
                return String.valueOf(8);
            case "organizačná zložka zahran. osoby":
                return String.valueOf(11);
            case "obecný podnik":
                return String.valueOf(14);
            case "obecný úrad":
                return String.valueOf(15);
        }
        return String.valueOf(0);
    }

    public static String getID_sud(String retazec) {
        switch (retazec) {
            case "Okresný Súd Banská Bystrica":
                return String.valueOf(3);
            case "Okresný Súd Bratislava I":
                return String.valueOf(2);
            case "Okresný Súd Košice I":
                return String.valueOf(4);
            case "Okresný Súd Nitra":
                return String.valueOf(9);
            case "Okresný Súd Prešov":
                return String.valueOf(8);
            case "Okresný Súd Trenčín":
                return String.valueOf(6);
            case "Okresný Súd Trnava":
                return String.valueOf(7);
            case "Okresný Súd Žilina":
                return String.valueOf(5);

        }
        return String.valueOf(0);
    }


    public static ObservableList<Firma> ConnectParse(String url) {
        System.out.println("Pripájanie ku stránke : " + url);


        ObservableList<Firma> firmy = FXCollections.observableArrayList();

        try {
            Document doc = Jsoup.connect(url).get();
            Element varovanie = doc.select("p.wrn").first();

            if (varovanie != null && !(varovanie.text().equals("Aplikácia neumožňuje výpis viac ako 500 položiek!"))) {
                AlertBox.display(varovanie.text());
            } else {
                Elements dalsieStrany = doc.select("td.bmk").select("a");
                int pocetStran = dalsieStrany.size();
                int dlzka = 0;
                int counter = 0;
                        do {
                            //Elements
                            Elements polozky = doc.select("div.sbj");
                            Elements cislo = doc.select("div.num");
                            Elements aktualny = doc.select("div.bmk").select("a:containsOwn(Aktuálny)");
                            Elements uplny = doc.select("div.bmk").select("a:containsOwn(Úplný)");

                            String nazovFirmy;
                            dlzka = cislo.size();


                            for (int i = 0; i < dlzka; i++) {

                                nazovFirmy = polozky.get(i).text();
                                if (nazovFirmy.contains("&amp;")) {
                                    nazovFirmy = nazovFirmy.replaceAll("&amp;", "&");
                                }
                                //System.out.println(poradie.text() + ": " + nazovFirmy + "  " + linkAktualny.text() + " " + linkUplny.text());
                                firmy.add(new Firma(cislo.get(i).text(),nazovFirmy,aktualny.get(i).attr("href"),uplny.get(i).attr("href")));
                            }

                            if(pocetStran != 0) {

                                doc = Jsoup.connect("http://orsr.sk/" + dalsieStrany.get(counter).attr("href")).get();
                            }

                            counter++;
                        }while (counter < pocetStran);

                    System.out.println(firmy.size());
                    return firmy;
            }




        } catch (IOException ex) {
            AlertBox.display("Chyba spojenia, skontrolujte pripojenie internetu!");
        }

        return null;

    }


}
