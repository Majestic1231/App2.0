package sample;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.*;
import java.net.URL;
import java.util.ResourceBundle;


public class TableController implements Initializable {

    @FXML
    private TableView<Firma> tabulka;
    @FXML
    private TableColumn<Firma, String> id;
    @FXML
    private TableColumn<Firma, String> nazovSubjektu;




    @Override
    public void initialize(URL location, ResourceBundle resources) {
        id.setCellValueFactory(new PropertyValueFactory<Firma, String>("poradie"));
        nazovSubjektu.setCellValueFactory(new PropertyValueFactory<Firma, String>("name"));
        ObservableList<Firma> zoznamFiriem = Controller.getZoznam();

        tabulka.getItems().setAll(zoznamFiriem);

    }

    public void changeSearchingMenuText(ActionEvent event) {
        MenuItem source = (MenuItem) event.getSource();
        ContextMenu moj = source.getParentPopup();
        MenuButton menu = (MenuButton) moj.getOwnerNode();
        menu.setText(source.getText());

    }


}
